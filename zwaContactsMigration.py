#!/usr/bin/env python

from collector import Collector
import pickle

class ContactsMigration:

	def __init__(self, collector):
		self.collector = collector


	def countries(self):
		self.collector.defineTable('countries')
		return self.collector.get();

	def findMsisdnCountryCode(self, msisdn, countries):

		countryCode = ''
		for country in countries:
			if(str(country['dialing_code']) == msisdn[0: len(str(country['dialing_code']))  ] ):
				countryCode = country['country_code']
				break

		return countryCode
				

	def migrate(self):

		progress = self.loadProgressTrack()
		lastId   = progress['lastContactsId']

		self.collector.defineTable('contacts')
		contacts = self.collector.where('id', '>', lastId).get()

		countries = self.countries()

		for contact in contacts:

			print "Now on contact id: " + str(contact['id']) + " \n"

			#prepared msisdn
			msisdn = self.prepareMsisdn(contact['msisdn'])

			#msisdn options
			msisdnOptions = self.msisdnOptions(msisdn)

			#msisdn options IDs
			msisdnOptionsIds = self.msisdnOptionsIds(msisdnOptions, contact['id'] )

			self.updateContactMsisdn(contact['id'], msisdn)

			if(len(msisdnOptionsIds) > 0 and len(set(progress['processedContactsIds']) & set(msisdnOptionsIds)) == 0):

				#update joining table for contacts and contact groups
				self.updateContactGroupsContactIds(msisdnOptionsIds, contact['id'])

				#update user contacts ids
				self.updateUserContactsContactIds(msisdnOptionsIds, contact['id'])

				#update joining table for users contacts and user contact groups
				self.updateUserContactGroupsContactIds(msisdnOptionsIds, contact['id'])

				#Finally delete the other contact options
				self.deleteMsisdnOptionsIds(msisdnOptionsIds)

				progress['processedContactsIds'].append(contact['id'])


			countryCode = self.findMsisdnCountryCode(msisdn, countries)

			#Update msisdn in the delivery log where msisdn options
			self.updateDeliveryLog(msisdnOptions, msisdn, countryCode)

			#update progress track
			progress['lastContactsId'] = contact['id']
			self.updateProgressTrack(progress)

		#clean up users contacts
		self.deleteDuplicateUserContacts()

		#clean up users contact group... a joining table
		self.deleteDuplicateUserContantGroup()

		#clean up contact group joining table
		self.deleteDuplicateContantGroup()


	def prepareMsisdn(self, msisdn, countryCode = ''):

		if(msisdn[0:2] == "00" ):
			return msisdn[2:]
		elif(msisdn[0:1] == "0" and countryCode):
			return countryCode + msisdn[2:]

		elif(msisdn[0:1] == "+" ):
			return msisdn[1:]
		else:
			return msisdn


	def msisdnOptions(self, msisdn):
		options = []
		options.append('00' + msisdn)
		options.append('+' + msisdn)
		options.append( msisdn )
		return options

	def updateContactMsisdn(self, id, msisdn):
		self.collector.defineTable('contacts')
		return self.collector.where('id', '=', id).update({"msisdn" : msisdn}).action()


	def msisdnOptionsIds(self, msisdnOptions, id):
		self.collector.defineTable('contacts')
		return self.collector.where('id', '!=', id).whereIn('msisdn', msisdnOptions ).lists('id')

	def updateContactGroupsContactIds(self, msisdnOptionsIds, contactId):
		self.collector.defineTable('contact_group')
		print self.collector.whereIn('contact_id', msisdnOptionsIds).update({'contact_id' : contactId}).action()

	def updateUserContactsContactIds(self, msisdnOptionsIds, contactId):
		self.collector.defineTable('user_contacts')
		self.collector.whereIn('contact_id', msisdnOptionsIds).update({'contact_id' : contactId}).action()

	def updateUserContactGroupsContactIds(self, msisdnOptionsIds, contactId):
		self.collector.defineTable('user_contact_group')
		self.collector.whereIn('contact_id', msisdnOptionsIds).update({'contact_id' : contactId}).action()

	def updateDeliveryLog(self, msisdnOptions, msisdn, countryCode):
		self.collector.defineTable('delivery_log')
		self.collector.whereIn('msisdn', msisdnOptions)
		self.collector.update({'msisdn' : msisdn, 'country_code' : countryCode }).action()

	def deleteMsisdnOptionsIds(self, msisdnOptionsIds):
		self.collector.defineTable('contacts')
		self.collector.whereIn('id', msisdnOptionsIds ).delete().action()

	def deleteDuplicateUserContacts(self):
		print "Deleting user contact  duplicates: " 

		progress = self.loadProgressTrack()
		lastId   = progress['lastUserContactId']

		self.collector.defineTable('user_contacts')
		rows = self.collector.where('id', '>', lastId).get()

		for row in rows:
			self.collector.defineTable('user_contacts')
			duplicatesIds  = self.collector.where('contact_id', '=', row['contact_id'] ).where('id', '!=', row['id'] ).lists('id')

			if( len(duplicatesIds) != 0 and len(set(progress['processedUserContactIds']) & set(duplicatesIds)) == 0 ):
				self.collector.defineTable('user_contacts')
				self.collector.whereIn('id', duplicatesIds).delete().action()
				progress['processedUserContactIds'].append(row['id'])
			
			progress['lastUserContactId'] = row['id']
			self.updateProgressTrack(progress)


	def deleteDuplicateUserContantGroup(self):
		print "Deleting user contact group duplicates: " 

		progress = self.loadProgressTrack()
		lastId   = progress['lastUserContactGroupId']

		self.collector.defineTable('user_contact_group')
		rows = self.collector.where('id', '>', lastId).get()

		for row in rows:
			self.collector.defineTable('user_contact_group')
			duplicatesIds  = self.collector.where('contact_id', '=', row['contact_id'] ).where('contact_group_id', '=', row['contact_group_id'] ).where('id', '!=', row['id'] ).lists('id')

			if(len(duplicatesIds) != 0 and len(set(progress['processedUserContactGroupIds']) & set(duplicatesIds)) == 0  ):
				self.collector.defineTable('user_contact_group')
				self.collector.whereIn('id', duplicatesIds).delete().action()
				progress['processedUserContactGroupIds'].append(row['id'])
			
			progress['lastUserContactGroupId'] = row['id']
			self.updateProgressTrack(progress)


	def deleteDuplicateContantGroup(self):

		print "Deleting contact group duplicates: " 

		progress = self.loadProgressTrack()
		lastId   = progress['lastContactGroupId']

		self.collector.defineTable('contact_group')
		rows = self.collector.get()

		processedIds = []

		for row in rows:
			self.collector.defineTable('contact_group')
			duplicatesIds  = self.collector.where('contact_id', '=', row['contact_id'] ).where('contact_group_id', '=', row['contact_group_id'] ).where('id', '!=', row['id'] ).lists('id')

			if ( len(duplicatesIds) != 0 and len(set(progress['processedContactGroupIds']) & set(duplicatesIds)) == 0 ) :
				self.collector.defineTable('contact_group')
				self.collector.whereIn('id', duplicatesIds).delete().action()
				progress['processedContactGroupIds'].append(row['id'])
			
			progress['lastContactGroupId'] = row['id']
			self.updateProgressTrack(progress)


	def loadProgressTrack(self):
		try:
			dataFile = open('progress-track.pkl', 'r')
			data   = pickle.load(dataFile)
			
		except IOError, e:
			data = {}

			data['lastContactsId']      = 0
			data['lastUserContactId']   = 0
			data['lastContactGroupId']  = 0
			data['lastUserContactGroupId']  = 0

 
			data['processedContactsIds'] = [0]
			data['processedUserContactIds'] = [0]
			data['processedContactGroupIds'] = [0]
			data['processedUserContactGroupIds'] = [0]

			self. updateProgressTrack(data)

		return  data


	def updateProgressTrack(self, data):
		dataFile = open('progress-track.pkl', 'w')
		pickle.dump(data, dataFile)
		dataFile.close()

				
migration = ContactsMigration(Collector())

migration.migrate()


