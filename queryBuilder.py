#!/usr/bin/env python

import re

class QueryBuilder():

	def __init__(self):

		self.queryType    = ''
		self.table        = ''
		self.queryString  = '' 
		self.whereString  = ''
		self.selectString = ''
		self.selectFields = ''
		self.updateCredsString = ''
		self.onString = ''

		self.insertKeysString = []
		self.insertValuesString = []

		self.onCredsString        = ''

		self.crossJoinTables = []
		self.innerJoinTables = []
		self.leftJoinTables = []
		self.params = []

	def defineTable(self, table = ''):
		self.table = table
		return self

	def insert(self, creds):
		self.queryType   =  "INSERT"
		self.insertKeysString   = ', '.join("{!s}".format(k) for(k) in creds.keys())
		self.insertValuesString = ', '.join("{!r}".format(k) for(k) in creds.values()) 
		return self


	def select(self, *selectFields):
		self.queryType    =  "SELECT"
		if(selectFields):
			self.selectFields = ', '.join(selectFields)
		return self

	def update(self, creds):
		self.queryType    =  "UPDATE"
		self.updateCredsString = ', '.join("{!s}={!r}".format(k, str(v)) for (k,v) in creds.items())
		#self.updateCredsString = ', '.join("{!s} = %s".format(k) for (k) in creds.keys())
		#self.params = self.params + ','.join("{!r}".format(k) for(k) in creds.values()).split(',')
		return self

	def delete(self):
		self.queryType    =  "DELETE"
		return self

	def where(self, field, sign , value):

		string = field + " " + sign + " %s  "
		
		self.params.append(value)

		if(self.whereString):
			self.whereString = self.whereString + " AND " + string + " "
		else:
			self.whereString = " WHERE " + string + " "
		return self

	def whereIn(self, field, valueList):

		listString = ', '.join("%s" for i in valueList)
		inString   = "(" + listString + ")"

		self.params = self.params  +   valueList
		
		if(self.whereString):
			self.whereString = self.whereString + " AND " + field + " IN " + inString + " "
		else:
			self.whereString = " WHERE " + field + " IN " + inString + " "
		
		return self


	def joinTablesString(self):
		joinString = '';

		if(self.innerJoinTables):
			joinString = joinString + ' INNER JOIN ' + ' INNER JOIN '.join(self.innerJoinTables)

		if(self.crossJoinTables):
			joinString = joinString + ' CROSS JOIN ' + ' CROSS JOIN '.join(self.crossJoinTables)

		if(self.leftJoinTables):
			joinString = joinString + ' LEFT JOIN ' + ' CROSS JOIN '.join(self.leftJoinTables)

		return joinString


	def innerJoin(self, table, field, value):
		self.innerJoinTables.append(table)
		self.where(field, '=', value)
		return self

	def crossJoin(self, table, field, value):
		self.crossJoinTables.append(table)
		self.where(field, '=', value)
		return self

	def leftJoin(self, table, field, value):
		self.leftJoinTables.append(table)
		self.where(field, '=', value)
		return self
		
	def on(self, table, field, sign,  value):
		self.onString  = " ON " + field + " " + sign + " " + value
		return self
 
	def determinQueryType(self):
		if(self.queryType != ''):
			return self.queryType
		else:
			return "SELECT"


	def buildQuery(self):

		if(self.determinQueryType() == "SELECT"):

			if(self.selectFields == ''):
				self.selectFields = ' * '

			self.queryString = "SELECT " + self.selectFields 
			self.queryString = self.queryString + " FROM " + self.table
			self.queryString = self.queryString + self.joinTablesString()
			self.queryString = self.queryString + self.onString
			self.queryString = self.queryString + self.whereString

		if(self.determinQueryType() == "UPDATE"):

			self.queryString = "UPDATE " + self.table 
			self.queryString = self.queryString + " SET " 
			self.queryString = self.queryString + self.updateCredsString 
			self.queryString = self.queryString + self.whereString

		if(self.determinQueryType() == "INSERT"):

			self.queryString = "INSERT INTO " + self.table 
			self.queryString = self.queryString + "(%s)" % self.insertKeysString
			self.queryString = self.queryString + " VALUES (%s)" % self.insertValuesString

		if(self.determinQueryType() == "DELETE"):

			self.queryString = "DELETE FROM " + self.table 
			self.queryString = self.queryString + self.whereString

		return self.queryString

	def reset(self):
		self.__init__()