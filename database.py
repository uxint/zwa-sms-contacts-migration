#!/usr/bin/env python

import MySQLdb
import MySQLdb.cursors

class Database:

	def __connection(self):
		try:
			conn = MySQLdb.connect(host = "localhost", user = "sk_admin", passwd = "oAwgFVjPcO", db = "sk_sms_system", cursorclass=MySQLdb.cursors.DictCursor)
			return conn
		except Exception, e:
			pass
		
	def query(self, queryString):
		conn   = self.__connection()
		cursor = conn.cursor()
		cursor.execute(queryString)
		conn.commit()
		return cursor

	def queryWithParams(self, queryString, params):
		conn   = self.__connection()
		cursor = conn.cursor()
		cursor.execute(queryString, params)
		conn.commit()
		return cursor

