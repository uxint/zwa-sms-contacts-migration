#!/usr/bin/env python

from queryBuilder import QueryBuilder;
from database  import Database;
import pickle

class Collector(Database, QueryBuilder):

	def action(self):
		if(len(self.params)):
			query = self.buildQuery()
			list = ','.join(str(i) for i in self.params).split(',')
			self.reset()
			return self.queryWithParams(query, tuple(list) )
		else:
			query = self.buildQuery()
			self.reset()
			return self.query(query)


	def get(self):
		return self.action().fetchall()

	def find(self):
		return self.action().fetchone()

	def lists(self, *list):

		rows   = self.get()
		output = [];

		if(len(list) < 2):
			for row in rows:
				output.append(row[list[0]])
		else:
			dict = {}
			for row in rows:
				for item in list:
					dict[item] = row[item]
					output.append(dict)


		return output

